from rdflib import URIRef, BNode, Literal, Namespace, RDF, OWL, Graph, FOAF, RDFS, SKOS
import pandas as pd
import logging
import shortuuid
import time

logging.basicConfig(level=logging.INFO, handlers=[logging.FileHandler("logs/logfile.log"),logging.StreamHandler()])
logger = logging.getLogger(__name__)

g = Graph()
nspace_doc =  Namespace("http://capgemini.com/covid-19_study/doc/")
nspace_bibTex=Namespace("https://zeitkunst.org/bibtex/0.2/bibtex.owl#")
nspace_people= Namespace("http://capgemini.com/covid-19_study/people/")
nspace_term = Namespace("http://capgemini.com/covid-19_study/vocabulary/terms/")
nspace_vocab = Namespace("http://capgemini.com/covid19/study/vocabulary/")
nspace_study = Namespace("http://capgemini.com/covid19/study/")
g.bind("study_doc", nspace_doc)
g.bind("foaf", FOAF)
g.bind("rdf",RDF)
g.bind("rdfs",RDFS)
g.bind("skos",SKOS)
g.bind("bibtex",nspace_bibTex)
g.bind("study_people",nspace_people)
g.bind("study_term",nspace_term)
g.bind("study_vocab",nspace_vocab)
g.bind("owl",OWL)
g.bind("study",nspace_study)


def author_exists(keyword,g):
    """This function checks if the author already exists as a person concept in graph 
    Parameters:
         keyword: Author keyword from the document
         g: The entire current graph in the database 
    Returns: keyword_exists boolean as True if the author already exists as a person in graph
    """
    logger.info("Checking if author_exists")
    keyword_exists = False
    if (None, FOAF.name, Literal(keyword)) in g:
        logger.info("This graph already has %s as a author_!",keyword)
        keyword_exists = True
    return keyword_exists

def term_exists(keyword,g):
    """This function checks if the vocabulary keyword already exists as a term concept in graph
    Parameters:
         keyword: Vocabulary keyword from abstract/title/body of document
         g: The entire current graph in the database 
    Returns: keyword_exists boolean as True if the keyword already exists as a term in graph
    """
    logger.info("Checking if term_exists")
    keyword_exists = False
    extracted_uri=""
    if (None, RDFS.label, Literal(keyword)) in g:
        logger.info("This graph already has %s as a term",keyword)
        for subject in g.subjects(predicate=RDFS.label, object=Literal(keyword)):
            extracted_uri= subject
        logger.info("Fetching Subject of this term %s",extracted_uri)
        keyword_exists = True
    return keyword_exists,extracted_uri

def create_document_triples(document,g):
    """This function creates the document class triple as a concept in the graph
    Parameters:
         document: Dataframe unique document ID
    Returns: Graph file appended with the document triples
    """
    logger.info("creating document node for %s",document)
    document_uri = URIRef(nspace_doc+str(document))
    logger.info("document_uri %s",document_uri)
    g.add((document_uri,RDF.type,URIRef(nspace_doc)))
    logger.info("document_uri setting name")
    g.add((document_uri,RDFS.label, Literal(document)))
    #logger.info("document_uri are instances of document class namespace")
    #g.add((document_uri,RDF.type, URIRef(nspace_doc)))
    logger.info("document_uri are subclasses of document class namespace")
    g.add((document_uri,RDFS.subClassOf, URIRef(nspace_doc)))
    return g

def create_author_triples(document_df,document,g):
    """This function creates the author triples as FOAF person
    Parameters:
         document_df: Dataframe containing author details of the document  
    Returns: Graph file appended with the document author nodes
    """
    logger.info("creating author nodes for %s",document)
    document_authors = document_df.loc[document_df['Section']=='has_author']
    logger.info("records list for authors of the document %s",document_authors)
    document_uri = URIRef(nspace_doc+str(document))
    for index in range(len(document_authors)):
        author = document_authors.iloc[index, 1].replace(" ", "") #keyword
        author_uri = URIRef(nspace_people+str(author))
        logger.info("author_uri %s",author_uri)
        logger.info("Document has the author  %s",author_uri)#Attaching document URI to the Author URI
        g.add((document_uri,nspace_bibTex.hasAuthor,author_uri))
        author_present = author_exists(author,g)
        if author_present == False:
            logger.info("Author %s is not existing in graph and thus we are creating seperate Person and Name Node for it",author)
            logger.info("Author is a person %s",URIRef(document_authors.iloc[index, 2]))
            g.add((author_uri,RDF.type,URIRef(document_authors.iloc[index, 2])))#person URI
            logger.info("Author has a name %s",Literal(author))
            g.add((author_uri,FOAF.name,Literal(author)))
    return g

def create_terms(document_df,document,g):
    """This function creates the vocabulary triples for medical concepts
    Parameters:
         document_df: Dataframe containing medical keywords from abstract, title and body of the document  
                      and its MeSH URI
    Returns: Graph file appended with the vocabulary triples
    """
    logger.info("creating terms for document  %s",document)
    document_keywords = document_df.loc[document_df['Section']!='has_author']
    logger.info("records list for keywords of the document %s",document_keywords)
    document_uri = URIRef(nspace_doc+str(document))
    for index in range(len(document_keywords)):
        keyword = document_keywords.iloc[index, 1].title() #keyword
        term_present,extracted_uri= term_exists(keyword,g)
        if term_present == False:
            logger.info("Term %s is not existing in graph and thus we are creating it",keyword)
            term_id=shortuuid.ShortUUID().random(length=4)
            keyword_mesh_uri = URIRef(document_keywords.iloc[index, 2]) #MeSH URI
            term_uri = URIRef(nspace_term+str("id-")+str(term_id))#can be nspace_study as well instead of extra term
            logger.info("Term URI   %s has the label %s",term_uri,keyword)
            g.add((term_uri,RDFS.label,Literal(keyword)))
            logger.info("Term URI   %s is defined by of %s",term_uri,keyword_mesh_uri)
            g.add((term_uri,RDFS.isDefinedBy,keyword_mesh_uri))
            logger.info("term_uri is subclasses of vocabulary term ")
            g.add((term_uri,RDFS.subClassOf, URIRef(nspace_vocab)))
            #logger.info("Term is a member of the document")
            #g.add((term_uri,RDFS.subClassOf, document_uri))
            logger.info("term_uri URI   %s is instance of document  %s",document_uri,term_uri)#
            g.add((term_uri,RDF.type,document_uri))#

        else:
            logger.info("Term %s exists and we fetched term URI to attach %s",keyword,extracted_uri)
            logger.info("term_uri is subclasses of vocabulary term ")
            g.add((extracted_uri,RDFS.subClassOf, URIRef(nspace_vocab)))
            #logger.info("Term is a member of the document")
            #g.add((extracted_uri,RDFS.subClassOf, document_uri))
            logger.info("term_uri URI   %s is instance of document  %s",document_uri,extracted_uri)#
            g.add((extracted_uri,RDF.type,document_uri))#
    return g

def create_graph(df):
    """This function sets the namespace and creates the Graph triples turtle and n-triples file
    Returns: Graph triples ready to be extracted in RDF format
    """
    logger.info("creating graph from the dataframe")
    docids= df.ID.unique()
    logger.info('unique_docids %s',docids)
    logger.info("study namespace class: creating")
    g.add((URIRef(nspace_study), RDF.type, OWL.Class))
    logger.info("study namespace class: labelling")
    g.add((URIRef(nspace_study), RDFS.label, Literal('Study')))
    logger.info("Document namespace class: creating")
    g.add((URIRef(nspace_doc), RDF.type, OWL.Class))
    logger.info("Document namespace subClass of Study")
    g.add((URIRef(nspace_doc), RDFS.subClassOf, URIRef(nspace_study)))
    logger.info("Document namespace Labelling")
    g.add((URIRef(nspace_doc), RDFS.label, Literal('Documents')))
    logger.info("vocabulary namespace class: creating")
    g.add((URIRef(nspace_vocab), RDF.type, OWL.Class))
    logger.info("term namespace subClass of Study")
    g.add((URIRef(nspace_vocab), RDFS.subClassOf, URIRef(nspace_study)))
    logger.info("term namespace Labelling")
    g.add((URIRef(nspace_vocab), RDFS.label, Literal('Vocabulary')))
    
    for document in docids:
        logger.info('**************fetching document for graph %s *****************',document)
        document_df = df.loc[df['ID'] == document]
        logger.info('records per document %s',document_df)
        graph_result = create_document_triples(document,g)
        graph_result = create_author_triples(document_df,document,g)
        graph_result = create_terms(document_df,document,g)
    return graph_result

def main():
    """This function reads the .csv file to prepare graph triples creation 
    extracted_keywords_data_graph.csv file is processed to create graph triples
    Returns: Graph triples in ntriples and turtle format
    """
    logger.info('********************Creating Knowledge Graph Triples********************')    
    logger.info('reading .csv for graph')    
    df = pd.read_csv("outputs/extracted_keywords_data_graph.csv")
    graph_result = create_graph(df)
    logger.info('displaying graph')
    timestamp = time.strftime("%Y%m%d-%H%M%S")
    logger.info(g.serialize(destination=f'outputs/graphs/graph_ntriples{timestamp}.nt',format="ntriples"))
    logger.info(g.serialize(destination=f'outputs/graphs/graph_turtle{timestamp}.ttl',format="turtle"))
    logger.info(g.serialize(format="ntriples").decode("utf-8"))
    logger.info(g.serialize(format="turtle").decode("utf-8"))


if __name__ == '__main__':
    main()