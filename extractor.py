'''
Entity extraction from JSON documents:

'''
import pke
import numpy as np
import time
import pandas as pd
import spacy
import re
import nltk
import string
import en_core_web_sm
import requests
import en_ner_bionlp13cg_md
import scispacy
import time
import logging
from rdflib import FOAF

logging.basicConfig(level=logging.DEBUG, handlers=[logging.FileHandler("logs/logfile.log"),logging.StreamHandler()])
logger = logging.getLogger(__name__)

nlp = spacy.load('en_core_web_sm')
nlp_bio= spacy.load('en_ner_bionlp13cg_md')

def get_spacy_entities(document):
    """This function displays word entities
    Parameters:
         model(module): A pretrained model from spaCy(https://spacy.io/models) or ScispaCy(https://allenai.github.io/scispacy/)
         document(str): Document to be processed
    Returns: Image rendering and list of named/unnamed word entities and entity labels
    """
    doc = nlp_bio(document)
    entity = [X.text for X in doc.ents]
    return entity

def extract_title(paperid,title):
    """This function extracts keywords from paper title
    Parameters:
         paperid: Unique document id
         title: Raw Text from paper title section
    Returns: Medical keywords as triples extracted from paper title
    """
    triples=[]
    triples_title_dummy=[]
    logger.debug('title of the paper is %s',title)
    logger.debug('extracting title from spacy bio NER model')
    title_bio = get_spacy_entities(title)
    for item in title_bio:
        triples_title_dummy.append(item)
    logger.debug('triples %s', triples_title_dummy)
    logger.debug('extracting title from spacymodel')
    title_spacy = nlp(title)
    for token in title_spacy:
        if((token.pos_ == "PROPN") or (token.pos_ == "NOUN")):
            if(len(token.text) > 2):
                triples_title_dummy.append(token.text)
    triples_title_dummy = list(set(i.lower() for i in triples_title_dummy))     
    for item in triples_title_dummy:
        logger.debug('adding token from title %s',item)
        l=[item, paperid, 'has_title']
        triples.append(l)
    logger.debug('triples after adding title %s,',triples)
    return triples

def extract_authors(paperid,authors,triples):
    """This function extracts Authors from research paper
    Parameters:
         paperid: Unique document id
         authors: Raw Text from paper author section
    Returns: Author Names as triples extracted from the document
    """
    for item in authors:
        name = item['first'] + " " +  item['last']
        logger.debug('authors token %s',name)
        l = [name, paperid, 'has_author']
        triples.append(l)
    logger.debug('triples after adding authors %s,',triples)
    return triples

def extract_abstract(paperid,abstract,triples):
    """This function extracts keywords from paper abstract
    Parameters:
         paperid: Unique document id
         abstract: Raw Text from paper abstract section
    Returns: Medical keywords as triples extracted from paper abstract
    """
    logger.debug('extracting abstract')
    abstract_text =""
    for item in abstract:
        abstract_text = abstract_text + item['text'] + " "
    logger.debug('abstract text %s,',abstract_text)
    abstract_spacy = re.sub(r" \d+", "", abstract_text)
    abstract_spacy = re.sub(r"[^A-Za-z0-9 -]+", "",abstract_spacy)
    abstract_spacy = nlp(abstract_spacy)
    list1=[]
    abstract_entities=abstract_spacy.ents
    logger.debug('abstract entities %s,',abstract_entities)
    for token in abstract_spacy:
        if((token.pos_ == "PROPN") or (token.pos_ == "NOUN")):
            list1.append(token.text)
    logger.debug('abstract list 1 %s,',list1)
    list2=[]
    for token in abstract_entities:
        list2.append(token.text)
    logger.debug('abstract list 2 %s,',list2)
    list3 = list(set(list1) | set(list2))
    logger.debug('abstract list 3 %s,',list3)
    abstract_bio = get_spacy_entities(abstract_text)
    for item in abstract_bio:
        logger.debug('abstract bio NER token %s',item)
        list3.append(item)
    list3 = list(set(i.lower() for i in list3))     
    for item in list3:
        if(len(item) > 2):
            l=[item, paperid, 'has_abstract']
            triples.append(l)
    logger.debug('triples after adding abstract %s,',triples)
    return triples

def extract_body(paperid,body,triples):
    """This function extracts keywords from paper body
    Parameters:
         paperid: Unique document id
         body: Raw Text from paper body section
    Returns: Medical keywords as triples extracted from document body
    """
    logger.debug('extracting body')
    body_text=""
    body_text_dummy=[]
    keyphrases = []
    for item in body:
        body_text = item['text']
        try:
            extractor = pke.unsupervised.TopicRank()
            extractor.load_document(input=body_text, language='en')
            extractor.candidate_selection()
            extractor.candidate_weighting()
            keyphrases = extractor.get_n_best(n=1)
            logger.debug('keyphrases from body %s', keyphrases)
        except ValueError:
            logger.debug('exception in phrase extractor %s : ', body_text)
            body_text = re.sub(r" \d+", "", body_text)
            body_text = re.sub(r"[^A-Za-z0-9 -]+", "",body_text)
            body_text=nlp(body_text)
            list1=[]
            body_text_entities = body_text.ents
            logger.debug('inside exception, entities from body %s :', body_text_entities)
            for token in body_text:
                if((token.pos_ == "PROPN") or (token.pos_ == "NOUN")):
                    if(len(token.text) > 2):
                        list1.append(token.text)
            logger.debug('inside exception, body list 1 :  %s', list1)  
            list2=[]
            for token in body_text_entities:
                list2.append(token.text)
            logger.debug('inside exception, body list 2 : %s', list2)  
            list3 = list(set(list1) | set(list2))
            logger.debug('inside exception, body list 3 : %s', list3)  
            for item in list3:
                x=[item]
                keyphrases.append(x)
        for item in keyphrases:
            logger.debug('appending keyphrases to body %s : ', item)  
            body_text_dummy.append(item[0])
    for item in body_text_dummy:
        l=[item, paperid, 'has_body']
        triples.append(l)
    logger.debug('triples after adding body %s,',triples)
    return triples

def extract_uri(triples,triples_with_uri):
    """This function extracts MeSH URI using MeSH API endpoint
    Parameters:
         triples: Keywords from research document
    Returns: Triples with corresponding MeSH URI for the extracted document keywords
    """
    count = 0
    for item in triples:
        keyword_exists = False
        if item[2] == 'has_author':
            logger.debug('Adding Author URI as Person : %s',item[0])
            l = [item[1],item[0],FOAF.Person, item[2]]
            triples_with_uri.append(l)
        else:
            entity = item[0]
            logger.debug('API search, entity is : %s',entity)
            for item_uri in triples_with_uri:
                if entity.lower()==item_uri[1].lower():
                    logger.debug('entity exists as keyword already]')
                    logger.debug('item_uri[1].lower() %s',item_uri[1].lower())
                    request_uri = item_uri[2]
                    l = [item[1],entity,request_uri, item[2]]
                    keyword_exists = True
                    logger.debug('Appending existing entity list, is : %s',l)
                    triples_with_uri.append(l)
                    break
            if keyword_exists == False:    
                try:
                    api_response = requests.get('https://id.nlm.nih.gov/mesh/lookup/term?label={query}&contains=exact&limit=1'.format(query=entity.strip()),
                    headers={"content-type": "application/json"}, verify=False,timeout=3)
                except requests.exceptions.HTTPError as errh:
                    logger.info("Http Error: %s",errh)
                except requests.exceptions.ConnectionError as errc:
                    logger.info("Error Connecting: %s",errc)
                except requests.exceptions.Timeout as errt:
                    logger.info("Timeout Error: %s",errt)
                except requests.exceptions.RequestException as err:
                    logger.info("Oops: Something Else %s",err)
                count=count+1
                logger.debug('request text : %s',eval(api_response.text))
                logger.debug('count : %s',count)
                if(count == 400):
                    count = 0
                    time.sleep(70)
                if api_response:
                    for x in eval(api_response.text):
                        request_uri = x['resource']
                        request_label = x['label']
                        l = [item[1],request_label,request_uri, item[2]]
                        triples_with_uri.append(l)
    return triples_with_uri