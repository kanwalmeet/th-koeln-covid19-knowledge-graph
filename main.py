import os
import json
import numpy as np
import time
import pandas as pd
import re
import nltk
import string
import logging
from extractor import extract_title, extract_abstract, extract_authors, extract_body,extract_uri
logging.basicConfig(level=logging.DEBUG, handlers=[logging.FileHandler("logs/logfile.log"),logging.StreamHandler()])
logger = logging.getLogger(__name__)


def main():
    """This is the main function which reads research articles function displays word entities
     Articles must be in JSON format and part of the data/input_folder/ 
    """
    logger.debug('Code begin')
    filelist=[]
    for dirname, _, filenames in os.walk('data/input_folder/'):
        for filename in filenames:
            filelist.append(os.path.join(dirname, filename))
    logger.info('file list %s',filelist)
    read_files(filelist)

def remove_common_words(triples):
    """This function removes common stop words from google corpora
    Parameters:
         triples: title, abstract, body keywords
    Returns: Removes the common words using google-english.txt from the triples list
    """
    logger.info('removing common words from the list of triples')
    google_words_list="google-english.txt"
    file = open(google_words_list,"r")
    google_words_file = file.readlines()
    word = []
    for token in google_words_file:
        word.append(token.replace("\n", ""))
    for item in triples:
        if (item[0].lower() in word):
            logger.debug('removing common word from google corpus:  %s,%s,',item[0],item[2])
            triples.remove(item)
    return triples

def read_files(filelist):  
    '''Processes each file one at time.
    Extracted title, abstract, author and
    body triples are saved in output folder
    as extracted_keywords_data_graph.csv 
    '''
    """Processes each file one at time.
    Parameters:
         filelist: JSON Documents extracted from input_folder
    Returns: Stores triples for knowledge graph in extracted_keywords_data_graph.csv file 
    """
    triples=[]
    triples_with_uri=[]
    start = time.time()
    logger.info('start time of program %s',start)
    for paper in filelist:
        with open(paper,"r") as object:
            data = object.read()
        logger.info('***********************************************************')    
        logger.info('reading of paper %s',paper)
        obj = json.loads(data)
        paperid = obj['paper_id']
        title = obj['metadata']['title']
        authors = obj['metadata']['authors']
        abstract = obj['abstract']
        bib = obj['bib_entries']
        body = obj['body_text']
        logger.info('paper_id %s',paperid)
        triples = extract_title(paperid,title)
        triples = extract_authors(paperid,authors,triples)
        triples = extract_abstract(paperid,abstract,triples)
        triples = extract_body(paperid,body,triples)
        triples = remove_common_words(triples)
        logger.info('reading of paper completed %s',paper)
        logger.debug('printing triples %s',triples)
        triples_with_uri = extract_uri(triples,triples_with_uri)
        logger.info('triples_with_uri]  : %s',triples_with_uri)
        logger.info('***********************************************************')
    end = time.time()
    logger.info('end time of program %s',end)
    logger.info('total elapsed time of program %s',end-start)
    dy = pd.DataFrame(triples_with_uri,columns=['ID', 'Keyword', 'URI','Section'])
    dy.drop_duplicates(['Keyword','ID'], inplace=True)
    dy.to_csv("outputs/extracted_keywords_data_graph.csv", index=False)
if __name__ == '__main__':
    main()